import core.RSA;
import org.omg.CosNaming.BindingHelper;
import sun.rmi.server.Util;
import utils.FileUtils;

import java.io.*;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

//        RSA rsa = new RSA(1024, true);
        RSA rsa = new RSA(1024, false);
        System.out.println(rsa.getKeysize());
        System.out.println("p: " + rsa.getP());
        System.out.println("q: " + rsa.getQ());
        System.out.println("e: " + rsa.getE());
        System.out.println("d: " + rsa.getD());
        System.out.println("public key:" + rsa.getKeypair().getPublicKey());
        System.out.println("private key:" + rsa.getKeypair().getPrivateKey());
        ////////////////////////////////
        ///////////////////////////////
        // Number encryption
//        BigInteger m = BigInteger.TEN;
//        System.out.println("m: " + m);
//        BigInteger c = m.modPow(rsa.getKeypair().getOpenExponent(), rsa.getKeypair().getModule());
//        System.out.println("c: " + c);
//        BigInteger md = c.modPow(rsa.getKeypair().getSecretExponent(), rsa.getKeypair().getModule());
//        System.out.println("md: " + md);
        ////////////////////////////////
        ////////////////////////////////
        // Text file encryption
//        try {
//            Path path = Paths.get("/home/alex/IdeaProjects/RSA_MD5_JAVA/src/resources/eth.bmp");
//            //Path path = Paths.get("/home/alex/IdeaProjects/RSA_MD5_JAVA/src/resources/test");
//            byte[] data = Files.readAllBytes(path);
//            BigInteger m = new BigInteger(data);
//            System.out.println(new String(m.toByteArray()));
//            BigInteger c = m.modPow(rsa.getKeypair().getOpenExponent(), rsa.getKeypair().getModule());
//            System.out.println(new String(c.toByteArray()));
//            BigInteger md = c.modPow(rsa.getKeypair().getSecretExponent(), rsa.getKeypair().getModule());
//            System.out.println(new String(md.toByteArray()));
//        } catch (IOException e) {
//            e.getMessage();
//        }

        try {
            Path path = Paths.get("/home/alex/IdeaProjects/RSA_MD5_JAVA/src/resources/eth.bmp");
            Path path2 = Paths.get("/home/alex/IdeaProjects/RSA_MD5_JAVA/src/resources/test");
            byte[] data = Files.readAllBytes(path);
            byte[] data2 = Files.readAllBytes(path2);
            byte[] sig1 = rsa.hmac(data);
            System.out.println("Signature passed: " + rsa.checkHMac(data, sig1));
            System.out.println("Signature passed: " + rsa.checkHMac(data2, sig1));
        } catch (IOException e) {
            e.printStackTrace();
        }
//        MessageDigest md = null;
//        try {
//            md = MessageDigest.getInstance("MD5");
//            InputStream is = Files.newInputStream(Paths.get("/home/alex/IdeaProjects/RSA_MD5_JAVA/src/resources/test"));
//            DigestInputStream di = new DigestInputStream(is, md);
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        byte[] digest = md.digest();
//        BigInteger bimd = new BigInteger(digest).abs();
////        if(bimd.compareTo(BigInteger.ZERO) < 0) bimd = bimd.multiply(new BigInteger("-1"));
//        System.out.println("Digest:" + bimd);
//        BigInteger bimdsig = rsa.powMod(bimd,rsa.getD(),rsa.getN());
//        System.out.println("(Digest)^d mod N:" + bimdsig);
//        BigInteger bimdchecksig = rsa.powMod(bimdsig, rsa.getE(), rsa.getN());
//        System.out.println("((Digest)^d)^e mod N: " + bimdchecksig);
//        if(new BigInteger(digest).abs().equals(bimdchecksig)) System.out.println("Signature ok");
//        else System.out.println("Signature failed...");

    }
}


// euclid (-1 mod for D)
// TODO file utils binary encryption/decryption
// TODO sign mode
// password key generation
// key generation