package utils;

import java.io.*;
import java.math.BigInteger;

public class FileUtils {

    private BigInteger[] message;
    private byte[][] bytes;
//    private String filename = "";
    private long filesize;
    private int chunkSize = 0;

    public FileUtils(int chunkSize, String filename) {
//        this.filename = filename;
        this.chunkSize = chunkSize;
        filesize = new File(filename).length();
        long chunks = filesize / chunkSize;
        bytes = new byte[(int)chunks+1][chunkSize];
        message = new BigInteger[(int)chunks+1];
    }

    public BigInteger[] readFile(String filename) throws IOException {

        FileInputStream reader = new FileInputStream(filename);
        int i = 0;
        int chk = 0;
        while (reader.read() != -1) {
            reader.read(bytes[i]);
            i++;
        }

        for (int j = 0; j < bytes.length; j++) {
            message[j] = new BigInteger(bytes[j]);
        }

        return message;
    }

    public void encryptFile(BigInteger exp, BigInteger mod, String out) {
        try {
            FileOutputStream writer = new FileOutputStream(out);
            for(BigInteger bi: message) {
                bi = bi.modPow(exp, mod);
                writer.write(bi.toByteArray());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void decryptFile(BigInteger exp, BigInteger mod, String out) {
        try {
            FileOutputStream writer = new FileOutputStream(out);
            for(BigInteger bi: message) {
                bi = bi.modPow(exp, mod);
                writer.write(bi.toByteArray());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

/*
RandomAccessFile reader = new RandomAccessFile(filename, "r");
        FileChannel channel = reader.getChannel();

        if(chunkSize > channel.size()) {
            chunkSize = (int) channel.size();
        }

        ByteBuffer buffer = ByteBuffer.allocate(chunkSize);
        int i = 0;
        while (channel.position() < reader.length()) {
            channel.read(buffer);
            buffer.flip();


        }

        //
        channel.close();
        reader.close();
*/