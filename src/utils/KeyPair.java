package utils;

import java.math.BigInteger;

public class KeyPair {
    private BigInteger e;
    private BigInteger d;
    private BigInteger n;

    public KeyPair(BigInteger e, BigInteger d, BigInteger n) {
        this.e = e;
        this.d = d;
        this.n = n;
    }

    public BigInteger getOpenExponent() {
        return e;
    }

    public BigInteger getSecretExponent() {
        return d;
    }

    public BigInteger getModule() {
        return n;
    }

    public String getPublicKey() {
        return "{" + e + ", " + n + "}";
    }

    public String getPrivateKey() {
        return "{" + d + ", " + n + "}";
    }
}
