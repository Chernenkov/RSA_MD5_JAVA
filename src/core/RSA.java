package core;
import utils.KeyPair;
import utils.Utils;

import java.io.*;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Random;

public class RSA {

    private static final double LOG2 = Math.log(2.0);

    private int keysize;
    private BigInteger p;
    private BigInteger q;
    private BigInteger n;
    private BigInteger eiler;
    private BigInteger e;
    private BigInteger d;
    private KeyPair keypair;
    private boolean qOk = false;
    private boolean pOk = false;
    private String password = "";

    public RSA(int keysize, boolean usePasswd) {
        long start = System.currentTimeMillis();
        System.gc();
        this.keysize = keysize;
        p = BigInteger.ZERO;
        q = BigInteger.ZERO;
        if(usePasswd) {
            System.out.println("Password usage is specified. Please, enter password: ");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            try { password = reader.readLine(); } catch (IOException e) {e.printStackTrace();}
        }
        System.out.println("Generating P...");
        calculateP();
        System.out.println("P generated...");
        System.gc();
        System.out.println("Generating Q...");
        calculateQ();
        System.out.println("Q generated...");
        System.gc();
        System.out.println("Generating N...");
        calculateN();
        System.out.println("N generated...");
        calculateEuler(p, q);
        calculateE();
        calculateD(e, eiler);
        keypair = new KeyPair(e, d, n);
        System.gc();
    }

    private void calculateP() {
        Random rand = new Random(System.currentTimeMillis());
        if(!password.equals("")) {
            rand = new Random(new BigInteger(password.getBytes()).longValue());
        }
        p = BigInteger.probablePrime(keysize, rand);
        pOk = isPrime(p, 10000);
        while(!pOk) {
            p = BigInteger.probablePrime(keysize, rand);
            pOk = isPrime(p, 10000);
        }
        System.out.println("P is prime: " + pOk);
    }

    private void calculateQ() {
        Random rand = new Random(System.currentTimeMillis());
        if(!password.equals("")) {
            rand = new Random(new BigInteger(password.getBytes()).longValue());
        }
        q = BigInteger.probablePrime(keysize, rand);
        qOk= isPrime(q,10000);
        while (!qOk) {
            q = BigInteger.probablePrime(keysize, rand);
            qOk= isPrime(q,10000);
        }
        System.out.println("Q is prime: " + qOk);
    }

    private void calculateN() {
        if((q.compareTo(BigInteger.ZERO) == 0) || (p.compareTo(BigInteger.ZERO) == 0) ) {
            throw new IllegalArgumentException("P or Q not generated yet");
        }
        n = q.multiply(p);
    }

    private void calculateEuler(BigInteger p, BigInteger q) {
        eiler = (p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));
    }

//    private BigInteger greatestCommonDivisor(BigInteger a, BigInteger b) {
//        return a.gcd(b);
//    }

    private void calculateE() {
        BigInteger gcd;
        for (e = BigInteger.valueOf(2); (e.compareTo(eiler) < 0); e = e.add(BigInteger.valueOf(System.currentTimeMillis()))) {
            gcd = e.gcd(eiler);
            if(gcd.compareTo(BigInteger.ONE) == 0) {
                return;
            }
        }
    }

    private void calculateD(BigInteger e, BigInteger eiler) {
        d = inverseMod(e, eiler);
    }

    // ax + by = gcd(a, b)
    private BigInteger[] extendedEuclid(BigInteger a, BigInteger b) {
        BigInteger[] answer = new BigInteger[3];
        BigInteger ax, by;

        if(b.equals(BigInteger.ZERO)) {
            answer[0] = a;
            answer[1] = BigInteger.ONE;
            answer[2] = BigInteger.ZERO;
            return answer;
        }

        answer = extendedEuclid(b, a.mod(b));
        ax = answer[1];
        by = answer[2];
        answer[1] = by;
        BigInteger tmp = a.divide(b);
        tmp = by.multiply(tmp);
        answer[2] = ax.subtract(tmp);
        return answer;
    }


    private BigInteger inverseMod(BigInteger a, BigInteger b) {
        BigInteger [] result = extendedEuclid(a, b);

        if(result[1].compareTo(BigInteger.ZERO) > 0) {
            return result[1];
        } else {
            return result[1].add(b);
        }
    }

    private double logBigInteger(BigInteger value) {
        int blex = value.bitLength() - 60; // 60..1023
        if(blex > 0) {
            value = value.shiftRight(blex);
        }
        double result = Math.log(value.doubleValue());
        return (blex > 0) ? (result + blex * LOG2) : result;
    }

//    boolean testMonteCarlo(BigInteger p) {
//        double log = logBigInteger(p);
//        BigInteger x = BigInteger.ONE;
//        while (log > 1) {
//            if(x.gcd(p).compareTo(BigInteger.ONE) > 0 || )
//            log--;
//        }
////        BigInteger r =
//    }

//    public BigInteger getProbPrime(int size, SecureRandom rnd) {
//
//    }

    public boolean isPrime(BigInteger n, int iteration)
    {
        // base case
        if (n.equals(BigInteger.ZERO) || n.equals(BigInteger.ONE))
            return false;
        // 2 is prime
        if (n.equals(new BigInteger("2")))
            return true;
        // an even number other than 2 is composite
        if (n.mod(new BigInteger("2")).equals(BigInteger.ZERO))
            return false;

        BigInteger s = n.subtract(new BigInteger("1"));
        while (s.mod(new BigInteger("2")).equals(BigInteger.ZERO))
            s = s.divide(new BigInteger("2"));

        Random rand = new Random();
        for (int i = 0; i < iteration; i++)
        {
            BigInteger r = new BigInteger(128, new SecureRandom());
            BigInteger a = r.mod(n.subtract(new BigInteger("1"))).add(new BigInteger("1"));
            BigInteger temp = s;
            BigInteger mod = a.modPow(temp, n);
            while (!(temp.equals(n.subtract(BigInteger.ONE))) &&
                    !(mod.equals(BigInteger.ONE)) &&
                    !(mod.equals(n.subtract(BigInteger.ONE)))) {

                mod = mod.modPow(mod, n); //mulMod(mod, mod, n);
                temp = temp.multiply(new BigInteger("2")); //temp * 2;
            }
            if (!(mod.equals(n.subtract(BigInteger.ONE)))
                    && (temp.mod(new BigInteger("2")).equals(BigInteger.ZERO)))
                return false;
        }
        return true;
    }


    /* Show */

    public String showParameters() {
        return "\np: " + p + "\nq: " + q + "\nn: "+ n + "\ne: " + e + "\nd: " + d + "\neiler: "+ eiler;
    }

    /* Getters */

    public BigInteger getD() {
        return d;
    }

    public BigInteger getE() {
        return e;
    }

    public BigInteger getEiler() {
        return eiler;
    }

    public BigInteger getN() {
        return n;
    }

    public BigInteger getP() {
        return p;
    }

    public BigInteger getQ() {
        return q;
    }

    public int getKeysize() {
        return keysize;
    }

    public KeyPair getKeypair() {
        return keypair;
    }


    ////////////////////////////////////////////////////////////////
    //////////////////// ENCRYPTION ////////////////////////////////
    ////////////////////////////////////////////////////////////////

    public byte[][] block(byte[] msg, int blkSz) {
        int nBlks = msg.length / blkSz;
        byte[][] ba = new byte[nBlks][blkSz];
        for (int i=0; i < nBlks; i++)
            for (int j=0; j < blkSz; j++)
                ba[i][j] = msg[i*blkSz + j];
        return ba;
    }

    public byte[] unblock(byte[][] ba, int blkSz) {
        byte[] ub = new byte [ba.length * blkSz];
        for (int i=0; i<ba.length; i++) {
            int j = blkSz-1, k = ba[i].length-1;
            while (k >= 0) {
                ub[i*blkSz+j] = ba[i][k];
                k--; j--;
            }
        }
        return ub;
    }

    public BigInteger powMod(BigInteger x, BigInteger n, BigInteger mod) {
        BigInteger res = BigInteger.ONE;
        for (BigInteger p = x; n.compareTo(BigInteger.ZERO) > 0; n = n.shiftRight(1), p = (p.multiply(p)).mod(mod)) {
            if (!(n.and(BigInteger.ONE)).equals(BigInteger.ZERO)) {
                res = (res.multiply(p).mod(mod));
            }
        }
        return res;
    }

    public byte[] hashMD5(byte[] message) {
        MessageDigest md5 = null;
        try {
             md5 = MessageDigest.getInstance("MD5");

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return md5 != null ? md5.digest(message) : new byte[0];
    }

    public byte[] hmac(byte[] message) {
        byte[] hash = hashMD5(message);
        BigInteger hashBI = new BigInteger(hash).abs();
        BigInteger signature = powMod(hashBI, getD(), getN());
        return signature.toByteArray();
    }

    public boolean checkHMac(byte[] message, byte[] signature) {
        BigInteger signBI = new BigInteger(signature);
        BigInteger h = powMod(signBI, getE(), getN());
        byte[] messageHash = hashMD5(message);
        BigInteger messageSignBI = new BigInteger(messageHash).abs();
        return messageSignBI.equals(h);
    }

}
